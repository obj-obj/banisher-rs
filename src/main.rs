#![feature(async_fn_in_trait)]
#![recursion_limit = "1000"]

mod global_name;

use chrono::{TimeZone, Utc};
use global_name::GlobalName;
use if_chain::if_chain;
use serde::Deserialize;
use serenity::{
    async_trait,
    client::bridge::gateway::GatewayIntents,
    futures::StreamExt,
    model::{event::ResumedEvent, gateway::Ready, prelude::*},
    prelude::*,
    utils::Color,
};
use std::{env, fs};
use tracing::{error, info};

pub const COLOR_MEMBER_JOIN: Color = Color::from_rgb(0, 255, 0);
pub const COLOR_MEMBER_LEAVE: Color = Color::from_rgb(255, 0, 0);

#[derive(Deserialize, Clone)]
struct Config {
    valid_usernames: Vec<String>,
    ignore_bots: bool,
}

impl Config {
    async fn get(ctx: &Context) -> Config {
        let data = ctx.data.read().await;
        data.get::<Config>().unwrap().clone()
    }
}

impl TypeMapKey for Config {
    type Value = Config;
}

async fn has_valid_username(ctx: &Context, config: &Config, user: &User) -> bool {
    let mut names = vec![user.name.clone()];
    if let Some(global_name) = user.global_name(ctx).await.unwrap() {
        names.push(global_name)
    }

    names.iter().any(|n| config.valid_usernames.contains(n))
        || user.id == ctx.cache.current_user().await.id
        || (config.ignore_bots && user.bot)
}

async fn get_guild_id_name(ctx: &Context, guild_id: &GuildId) -> String {
    guild_id
        .name(ctx)
        .await
        .unwrap_or_else(|| String::from("(unknown server)"))
}

async fn send_system_channel_embed(
    ctx: &Context,
    guild_id: &GuildId,
    description: &str,
    color: Color,
) -> serenity::Result<channel::Message> {
    if_chain! {
        if let Some(guild) = guild_id.to_guild_cached(ctx).await;
        if let Some(system_channel_id) = guild.system_channel_id;
        then {
            system_channel_id.send_message(ctx, |m| {
                m.embed(|e| {
                    e.description(description);
                    e.color(color);
                    e
                });
                m
            }).await
        } else {
            Err(serenity::Error::Other("No system channel"))
        }
    }
}

async fn kick_forbidden_username(
    ctx: &Context,
    member: &Member,
    valid_usernames: &[String],
    is_on_join: bool,
) {
    let guild_name = get_guild_id_name(ctx, &member.guild_id).await;
    let message_option = {
        let mut valid_usernames_str = String::from("`");
        valid_usernames_str.push_str(&valid_usernames.join("`, `"));
        valid_usernames_str.push('`');
        let kick_message = format!(
            "You have been kicked from {} because your username is not one of the following: {}",
            guild_name, valid_usernames_str,
        );
        member
            .user
            .direct_message(ctx, |m| m.content(kick_message))
            .await
            .ok()
    };
    let result = member.kick_with_reason(ctx, "Invalid username").await;
    if result.is_ok() {
        info!("Kicked {} from {}", member.user.tag(), guild_name);
        let description = match is_on_join {
            true => format!(
                "{} was prevented from joining the server.",
                member.user.tag()
            ),
            false => format!("{} was kicked from the server.", member.user.tag()),
        };
        send_system_channel_embed(ctx, &member.guild_id, &description, COLOR_MEMBER_LEAVE)
            .await
            .ok();
    } else {
        error!(
            "Failed to kick {} from {}: {:?}",
            member.user.tag(),
            guild_name,
            result
        );
        if let Some(message) = message_option {
            message.delete(ctx).await.ok();
        }
    }
}

async fn scan_guild(ctx: &Context, guild_id: &GuildId) {
    let config = Config::get(ctx).await;
    let current_member = guild_id
        .member(ctx, ctx.cache.current_user().await)
        .await
        .unwrap();
    let guild_name = get_guild_id_name(ctx, guild_id).await;
    if !current_member
        .permissions(ctx)
        .await
        .unwrap()
        .kick_members()
    {
        error!("Missing KICK_MEMBERS permission in {}", guild_name);
        return;
    }
    let mut members = guild_id.members_iter(ctx).boxed();
    while let Some(member_result) = members.next().await {
        if let Ok(member) = member_result {
            if !has_valid_username(ctx, &config, &member.user).await {
                info!(
                    "Attempting to kick {} from {} in scan_guild",
                    member.user.tag(),
                    guild_name
                );
                kick_forbidden_username(ctx, &member, &config.valid_usernames, false).await;
            }
        }
    }
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);
        ctx.set_activity(Activity::watching("usernames")).await;
        for guild_id in ctx.cache.guilds().await {
            scan_guild(&ctx, &guild_id).await;
        }
    }

    async fn resume(&self, ctx: Context, _: ResumedEvent) {
        info!("Resumed");
        ctx.set_activity(Activity::watching("usernames")).await;
    }

    async fn guild_member_addition(&self, ctx: Context, guild_id: GuildId, new_member: Member) {
        let config = Config::get(&ctx).await;
        if has_valid_username(&ctx, &config, &new_member.user).await {
            send_system_channel_embed(
                &ctx,
                &guild_id,
                &format!(
                    "{} ({}) joined the server.",
                    new_member.mention(),
                    new_member.user.tag()
                ),
                COLOR_MEMBER_JOIN,
            )
            .await
            .ok();
        } else {
            let guild_name = get_guild_id_name(&ctx, &guild_id).await;
            info!(
                "Attempting to kick {} from {} in guild_member_addition",
                new_member.user.tag(),
                guild_name
            );
            kick_forbidden_username(&ctx, &new_member, &config.valid_usernames, true).await;
        }
    }

    async fn guild_member_removal(
        &self,
        ctx: Context,
        guild_id: GuildId,
        user: User,
        _: Option<Member>,
    ) {
        let config = Config::get(&ctx).await;
        if has_valid_username(&ctx, &config, &user).await {
            send_system_channel_embed(
                &ctx,
                &guild_id,
                &format!("{} left the server.", user.tag()),
                COLOR_MEMBER_LEAVE,
            )
            .await
            .ok();
        }
    }

    async fn presence_update(&self, ctx: Context, new_data: PresenceUpdateEvent) {
        let config = Config::get(&ctx).await;
        if_chain! {
            if let Some(guild_id) = new_data.guild_id;
            if let Ok(member) = guild_id.member(&ctx.http, new_data.presence.user_id).await; // don't use cache
            if !has_valid_username(&ctx, &config, &member.user).await;
            let joined_at = match member.joined_at {
                Some(t) => t,
                None => Utc.timestamp(0, 0)
            };
            if (Utc::now() - joined_at).num_seconds() >= 1;
            then {
                let guild_name = get_guild_id_name(&ctx, &guild_id).await;
                info!("Attempting to kick {} from {} in presence_update", member.user.tag(), guild_name);
                kick_forbidden_username(&ctx, &member, &config.valid_usernames, false).await;
            }
        }
    }
}

#[tokio::main]
async fn main() {
    dotenv::dotenv().expect("Failed to load .env file");
    tracing_subscriber::fmt::init();

    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .intents(GatewayIntents::all())
        .await
        .expect("Error creating client");

    {
        let config: Config =
            toml::from_str(&fs::read_to_string("bot.toml").expect("Failed to read bot.toml"))
                .expect("Failed to parse bot.toml");

        let mut data = client.data.write().await;
        data.insert::<Config>(config);
    }

    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}
