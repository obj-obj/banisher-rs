use serde::Deserialize;
use serenity::{
    http::{request::RequestBuilder, routing::RouteInfo, Http},
    model::prelude::*,
};

pub trait GlobalName {
    /// Gets the user's global name (aka display name) if set.
    async fn global_name(&self, http: impl AsRef<Http>) -> Result<Option<String>, serenity::Error>;
}

#[non_exhaustive]
#[derive(Deserialize)]
struct UserWithGlobalName {
    global_name: Option<String>,
}

impl GlobalName for User {
    async fn global_name(&self, http: impl AsRef<Http>) -> Result<Option<String>, serenity::Error> {
        let route_info = RouteInfo::GetUser { user_id: self.id.0 };
        let request = RequestBuilder::new(route_info);

        http.as_ref()
            .fire::<UserWithGlobalName>(request.build())
            .await
            .and_then(|u| Ok(u.global_name))
    }
}
